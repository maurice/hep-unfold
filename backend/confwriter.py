'''
Write XML config files for EWUnfolding backend.
'''

import xml.etree.ElementTree as ET



# Return an XML-formatted string representing the settings
#
# Setting names of the form "Iteration__*" will be truncated to "Iteration"
# (The reason they have different names in the first place is that the dictionary
# keys must be unique)
#
def make_xml(name, settings):
    block = ET.Element(name)
    for setting_rawname, value in settings.iteritems():
        setting = setting_rawname.split("__")[0]
        if isinstance(value, dict):
            # Gives nested block
            block.append(make_xml(setting, value))
        else:
            # Gives simple subelement in the parent block
            ET.SubElement(block, setting).text = value
    return block



def write(name, settings, file):
    xml_string = ET.tostring(make_xml(name, settings)).replace("><", ">\n<")
    file.write(xml_string + "\n\n")
