#! /usr/bin/env python

from hepunfold import *

binning = [100, 150, 200, 250, 300, 350, 400, 500, 1000]

m = Model("example", binning, default_histfile="input_signal.root")

m.add_variation("nominal", "nominal/response", ["nominal/bkg1", "nominal/bkg2"])
m.add_variation("variation_up", "variation_up/response", ["variation_up/bkg1", "variation_up/bkg2"])
m.add_variation("variation_down", "variation_down/response", ["variation_down/bkg1", "variation_down/bkg2"])
m.add_variation("another", "another/response", ["another/bkg1", "another/bkg2"], "up")

unfold("input_data.root:reconstructed", m, "Bayesian3", mc_as_data=True, xmethod="BinByBin", normalized=False, debug=True)
