#! /usr/bin/env python

import os
import shutil
import sys

all_variations = [
'nominal',
'JVT_EFF__1up',
'JVT_EFF__1down',
'PRW_DATASF__1down',
'PRW_DATASF__1up',
'GENERATOR__1up',
'GENERATOR__1down',
'DD__1up',
'DD__1down',
'XSECT__1up',
'XSECT__1down',
'PDF__1up',
'PDF__1down',
'QCD_SCALE__1up',
'QCD_SCALE__1down',
'EL_EFF_Iso__1up',
'EL_EFF_Iso__1down',
'EL_EFF_Reco__1up',
'EL_EFF_Reco__1down',
'EL_EFF_ID__1down',
'EL_EFF_ID__1up',
'EG_RESOLUTION_ALL__1down',
'EG_RESOLUTION_ALL__1up',
'EG_SCALE_ALL__1down',
'EG_SCALE_ALL__1up',
'JET_BJES_Response__1down',
'JET_BJES_Response__1up',
'JET_EffectiveNP_1__1down',
'JET_EffectiveNP_1__1up',
'JET_EffectiveNP_2__1down',
'JET_EffectiveNP_2__1up',
'JET_EffectiveNP_3__1down',
'JET_EffectiveNP_3__1up',
'JET_EffectiveNP_4__1down',
'JET_EffectiveNP_4__1up',
'JET_EffectiveNP_5__1down',
'JET_EffectiveNP_5__1up',
'JET_EffectiveNP_6__1down',
'JET_EffectiveNP_6__1up',
'JET_EffectiveNP_7__1down',
'JET_EffectiveNP_7__1up',
'JET_EffectiveNP_8restTerm__1down',
'JET_EffectiveNP_8restTerm__1up',
'JET_EtaIntercalibration_Modelling__1down',
'JET_EtaIntercalibration_Modelling__1up',
'JET_EtaIntercalibration_NonClosure__1down',
'JET_EtaIntercalibration_NonClosure__1up',
'JET_EtaIntercalibration_TotalStat__1down',
'JET_EtaIntercalibration_TotalStat__1up',
'JET_Flavor_Composition__1down',
'JET_Flavor_Composition__1up',
'JET_Flavor_Response__1down',
'JET_Flavor_Response__1up',
'JET_JER_SINGLE_NP__1up',
'JET_Pileup_OffsetMu__1down',
'JET_Pileup_OffsetMu__1up',
'JET_Pileup_OffsetNPV__1down',
'JET_Pileup_OffsetNPV__1up',
'JET_Pileup_PtTerm__1down',
'JET_Pileup_PtTerm__1up',
'JET_Pileup_RhoTopology__1down',
'JET_Pileup_RhoTopology__1up',
'JET_PunchThrough_MC15__1down',
'JET_PunchThrough_MC15__1up',
'JET_SingleParticle_HighPt__1down',
'JET_SingleParticle_HighPt__1up',
'MUON_EFF_STAT_LOWPT__1down',
'MUON_EFF_STAT_LOWPT__1up',
'MUON_EFF_STAT__1down',
'MUON_EFF_STAT__1up',
'MUON_EFF_SYS_LOWPT__1down',
'MUON_EFF_SYS_LOWPT__1up',
'MUON_EFF_SYS__1down',
'MUON_EFF_SYS__1up',
'MUON_ID__1down',
'MUON_ID__1up',
'MUON_ISO_STAT__1down',
'MUON_ISO_STAT__1up',
'MUON_ISO_SYS__1down',
'MUON_ISO_SYS__1up',
'MUON_MS__1down',
'MUON_MS__1up',
'MUON_SAGITTA_RESBIAS__1down',
'MUON_SAGITTA_RESBIAS__1up',
'MUON_SAGITTA_RHO__1down',
'MUON_SAGITTA_RHO__1up',
'MUON_SCALE__1down',
'MUON_SCALE__1up',
'MUON_TTVA_STAT__1down',
'MUON_TTVA_STAT__1up',
'MUON_TTVA_SYS__1down',
'MUON_TTVA_SYS__1up',
]

binnings = {
    'fourLepton_mass_binning0' : [140, 180, 200, 220, 240, 260, 280, 300, 325, 350, 400, 500, 600, 800, 1500],
    'fourLepton_pT_binning0' : [0, 5, 15, 25, 35, 45, 55, 65, 75, 85, 100, 125, 150, 200, 250, 1500],
    'leadingDilepton_pT_binning0' : [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 200, 250, 1500],
    'subleadingDilepton_pT_binning0' : [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 120, 140, 160, 200, 250, 1500],
    'lepton1_pT_binning0' : [20, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 180, 200, 230, 450],
    'lepton2_pT_binning0' : [15, 40, 50, 60, 70, 80, 90, 100, 120, 150, 300],
    'lepton3_pT_binning0' : [10, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 80, 100, 200],
    'lepton4_pT_binning0' : [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 150],
    'fourLepton_absy_binning0' : [0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 10],
    'dileptons_dy_binning0' : [0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.5, 2.0, 2.5, 3.0, 10.0],
    'dileptons_dphi_binning0' : [0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.6875, 0.75, 0.8125, 0.875, 0.9375, 1.0],
    'jets_N_binning0' : [-0.5, 0.5, 1.5, 2.5, 3.5, 10.5],
    'centralJets_N_binning0' : [-0.5, 0.5, 1.5, 2.5, 3.5, 10.5],
    'jets60_N_binning0' : [-0.5, 0.5, 1.5, 2.5, 10.5],
    'hardestJetsDijet_mass_binning0' : [0, 50, 100, 200, 300, 1000],
    'hardestJetsDijet_dy_binning0' : [0, 1, 2, 3, 9],
    'jets_scalarPtSum_binning0' : [30, 60, 90, 120, 150, 200, 400, 1000],
    'jet1_pt_binning0' : [30, 40, 50, 60, 80, 100, 120, 150, 200, 800],
    'jet2_pt_binning0' : [30, 40, 60, 500],
    'jet1_eta_binning0' : [0, 0.5, 1.0, 1.5, 2.0, 2.5, 4.5],
    'jet2_eta_binning0' : [0, 0.5, 1.0, 1.5, 2.0, 2.5, 4.5],
}


from hepunfold import *

def make_name(systname):
    tokenized = systname.split('__')
    postfix = '__' + tokenized[-1] if len(tokenized) > 1 else ''
    return tokenized[0].replace('_', '') + postfix

def recreatedir(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)

def write_configs(prefix, obs, variations, signalfile, datafile, bkgfile, method, altgenfile=''): # , ddclosurefile=''
    for obs, binning in obs.iteritems():
        for channel in ['NoChannelSelection']: # , 'ElectronChannel', 'MixedChannel', 'MuonChannel'
            identifier = '_'.join([prefix, method, obs, channel])
            m = Model(identifier, binning, default_histfile=signalfile)
    
            for variation in variations:
                
                basepath = '/'.join([channel, variation, 'Unfolding', obs])
                nominalpath = '/'.join([channel, 'nominal', 'Unfolding', obs]) # like basepath but with variation replaced with nominal
                
                bkg_variation = variation if variation.split('__')[0] in ['XSECT', 'DD'] else 'nominal'
                
                nicename = make_name(variation)
                if 'MUONEFFSTATLOWPT' in nicename: nicename = 'MUONEFFLOWPTSTAT__' + nicename.split('__')[-1]
                if 'MUONEFFSYSLOWPT' in nicename: nicename = 'MUONEFFLOWPTSYS__' + nicename.split('__')[-1]

                if variation == 'GENERATOR__1up':
                    if not altgenfile: raise RuntimeError('Please provide an alternative generator file so that I can do that systematic!')
                    # NO BACKGROUND! We only use *signal* of the alternative generator!
                    bkgprediction = [] # if not bkgfile else [bkgfile + ':' + nominalpath + '/reconstructed']
                    m.add_variation(nicename, altgenfile+':'+nominalpath, bkgprediction)
                elif variation == 'GENERATOR__1down':
                    m.add_variation(nicename, nominalpath, bkgprediction)
                else:
                    bkgprediction = [] if not bkgfile else [bkgfile + ':' + channel + '/' + bkg_variation + '/Unfolding/' + obs + '/reconstructed']
                    m.add_variation(nicename, basepath, bkgprediction)

                if variation == 'JET_JER_SINGLE_NP__1up':
                    bkgprediction = [] if not bkgfile else [bkgfile + ':' + nominalpath + '/reconstructed']
                    m.add_variation(nicename.replace('__1up', '__1down'), basepath.replace('JET_JER_SINGLE_NP__1up', 'nominal'), bkgprediction, up_or_down='Down')

            if 'ddclosure' in prefix:
                unfold(datafile + ':' + obs, m, method, normalized=False, mc_as_data=False, xmethod='BinByBin', debug=True, mcuncert=True)
            else:
                unfold(datafile + ':' + channel + '/nominal/Unfolding/' + obs + '/reconstructed', m, method, normalized=False, mc_as_data=False, xmethod='BinByBin', debug=True, mcuncert=True)

def main():
    recreatedir('unfolding_configs')
    
    variations = all_variations
    
    methods = ['Bayesian' + str(i+1) for i in range(3)] + ['BinByBin']
    
    path = "/home/mauriceb/workspace_ZZ_2016/AnalysisProject/input/input_EW_stefan/"
    signalfile = path+'processed_sherpasmsignal_Signal_systematics.root'
    bkgfile = path+'processed_totalbkg_Signal_default.root'
   
    if not 'systematics' in signalfile:
        variations = [
            'nominal',
            'XSECT__1up',
            'XSECT__1down',
            'GENERATOR__1up',
            'GENERATOR__1down',
        ]

    for method in methods:
        # Data unfolding:
#         write_configs('data', binnings, variations, signalfile, path+'data.root', bkgfile, method)
        # Closure:
        write_configs('mcclosure', binnings, ['nominal'], signalfile, signalfile, bkgfile, method)
        # Unfolding method uncertainty:
#         write_configs('ddclosure', binnings, ['nominal'], signalfile, ddclosurefile, '', method)
#         write_configs('binbybinddclosure', binnings, ['nominal'], signalfile, binbybinddclosurefile, '', method)
    
    recreatedir('unfolding_configs')
    os.system('mv *_config.xml unfolding_configs/')

if __name__ == '__main__':
    main()
