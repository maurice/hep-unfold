'''
Python module for unfolding. Create unfolded distributions and
meta information, such as uncertainties and migration matrices.

Required inputs are histograms of data, signal and background
predictions, and meta-info for the signal. Systematic
uncertainties are supported, they are taken into account by
providing histograms for each systematic variation.

This module is only a frontend. When the unfolding is set up, it
writes configuration files (in XML) and histogram files (in
ROOT) into a new subdirectory. When the unfolding algorithm is
executed, an external executable is called for these input
files. 
'''

import helpers
import backend.confwriter as confwriter



# TODO: binning could be inferred automatically from any of the given histograms!
class Model:
    def __init__(self, identifier, binning, default_histfile="", responsenames="default"):
        '''
        Constructor
        '''
        self.default_histfile = default_histfile
        self.generalinfo = {
            "Identifier" : identifier,
            "SelectedChannel" : "-1", # this setting is not needed as far as I'm aware
            "Binning" : ", ".join([str(edge) for edge in binning]),
            }
        self.responsenames = helpers.set_responsenames(responsenames)
        self.responsenames["purityDenom"] = self.responsenames["fidCorrDenom"]
        self.sinfo = {
            "ReadHistograms" : "True",
            }
        self.binfo = {}
        self.datainfo = {}



    def add_variation(self, name, spath, bpaths, up_or_down="automatic", verbose=False):
        '''
        Add signal and background predictions for one systematic variation
        '''
        # The nominal variation is treated in a slightly special way
        if name.lower() == "nominal":
            svariation = {
                "Name" : "Nominal",
                "FileName" : spath.split(":")[0] if ":" in spath else self.default_histfile,
                "DirectoryName" : spath.split(":")[-1],
                }
            self.sinfo["Iteration__" + name] = svariation
            for bpath in bpaths:
                bvariation = {
                    "Name" : "Nominal",
                    "FileName" : bpath.split(":")[0] if ":" in bpath else self.default_histfile,
                    "HistogramName" : bpath.split(":")[-1],
                    }
                self.binfo["Iteration__" + name] = bvariation
        else:
            up_or_down = up_or_down.title() if up_or_down.lower() in ["up", "down"] else helpers.determine_up_or_down(name)
            # Take only the first part of name if it ends with '__1up' or something similar,
            # so that the name will be the same for the up and down variation: 
            name_same_for_up_and_down = name.split("__")[0]
            svariation = {
                "Name" : name_same_for_up_and_down,
                "FileName" : spath.split(":")[0] if ":" in spath else self.default_histfile,
                "DirectoryName" : spath.split(":")[-1],
                "Variation" : up_or_down
                }
            self.sinfo["Iteration__" + name] = svariation
            for bpath in bpaths:
                bvariation = {
                    "Name" : name_same_for_up_and_down,
                    "FileName" : bpath.split(":")[0] if ":" in bpath else self.default_histfile,
                    "HistogramName" : bpath.split(":")[-1],
                    "Variation" : up_or_down
                    }
                self.binfo["Iteration__" + name] = bvariation



    def finalize(self):
        '''
        Finalise the model and write config files.
        '''
        if not self.binfo:
            self.binfo["NoBackground"] = "True"
        ### Write out EWUnfolding config for model
        with open(self.generalinfo["Identifier"] + "_config.xml", "w") as configfile:
            confwriter.write("GeneralInformation", self.generalinfo, configfile)
            confwriter.write("DataInformation", self.datainfo, configfile)
            confwriter.write("MCHistogramInformation", self.responsenames, configfile)
            confwriter.write("MCInformation", self.sinfo, configfile)
            confwriter.write("BackgroundInformation", self.binfo, configfile)



class Result:
    def __init__(self, identifier):
        '''
        Constructor
        '''
        self.identifier = identifier
        pass



def read_result(rootfile):
    return Result("")



def unfold(datahistpath, model, method, normalized=True, mcuncert=True, absolute=False, mc_as_data=False, xmethod="", debug=False):
    ### Complete general info of the model
    model.generalinfo["UnfoldingMethod"] = method
    model.generalinfo["Normalized"] = str(normalized)
    model.generalinfo["UseAbsoluteValues"] = str(absolute)
    model.generalinfo["Debug"] = str(debug)
    if xmethod:
        model.generalinfo["CrossCheckMethod"] = xmethod
    model.generalinfo["StatUncertaintyAlternatives"] = "True"
    model.generalinfo["PositionOfToyMCReplicaNumber"] = "IterationDirectory"
    model.generalinfo["OutputFileName"] = "results_" + model.generalinfo["Identifier"] + ".root"
    ### Complete signal MC info of the model
    model.sinfo["ConsiderMCUncertainty"] = str(mcuncert)
    ### Complete data info of the model
    model.datainfo["UseStatUncFromData"] = "True"
    model.datainfo["UseFastStatEstimate"] = "True"
    model.datainfo["FileName"] = datahistpath.split(":")[0]
    model.datainfo["HistogramName"] = datahistpath.split(":")[1]
    model.datainfo["UseMCAsData"] = str(mc_as_data)
    ### Finalise the model and write out config file
    model.finalize()
    ### TODO: call EWUnfolding!!!
    return Result(model.generalinfo["Identifier"])



def check_closure(result, tolerance=0.000001):
    # TODO
    pass


